<?php

/**
 * 
 */
class Support_lib  {
	private $ci;
	function __construct() {
		$this->ci = &get_instance();
		$this->ci->lang->load('general', 'english');
	}
	public function view_template($data)
	{
		$this->ci->load->view('view_controller',$data);
	}
	
	public function view_cpanel($data)
	{
		$this->ci->load->view('cpanel_controller',$data);
	}
	
	public function get_current_date()
	{
		return date('Y-m-d H:i:s');
	}
	
	public function get_noti_msg($key='')
	{
		if($key == 'assign_paper'){
			return $this->ci->lang->line('assign_paper_not_msg');
		}
	}
}
