<?php

/**
 * 
 */
class Data_model extends CI_Model {
	
	public function get_questions()
	{
		$q = $this->db->get('questions');
		if($q->num_rows() > 0){
			return $q->result();
		}
		else {
			return FALSE;
		}
	}
	public function increment_test_result($type='')
	{
		$q = $this->db->get_where('statistics',array('name'=>$type.'_test_results'));
		$num = $q->row()->value;
		$num++;
		$this->db->where(array('name'=>$type.'_test_results'));
		$this->db->update('statistics',array('value'=>$num));
	}
	public function get_test_result($type='')
	{
		$q = $this->db->get_where('statistics',array('name' =>$type.'_test_results'));
		return $q->row()->value;
	}
}
