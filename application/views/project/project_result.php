<style>
	.printtable * { display : none; }
	.printtable table { display : block; }
</style>
<script>
	function printDiv(divName) {
	     var printContents = document.getElementById(divName).innerHTML;
	     var originalContents = document.body.innerHTML;
	
	     document.body.innerHTML = printContents;
	
	     window.print();
	
	     document.body.innerHTML = originalContents;
	}
</script>
<div class="container">
	<?php 
	$page = '<div id="print_area"><h3>'. $pro_name.'</h3>
	<p>'.$description.'</p>
	<table class="table col-xs-12">
		<tr class="row">
			<td class="col-xs-3" >
				Project Goal
			</td>
			<td class="col-xs-3" >
				'.$pro_goal.'
			</td>	
		</tr>
		<tr class="row">
			<td class="col-xs-3" >
				Assigned To
			</td>
			<td class="col-xs-3" >
				'.$assigned_to.'
			</td>	
		</tr>
		<tr class="row">
			<td class="col-xs-3" >
				Start Date
			</td>
			<td class="col-xs-3" >
				'.$s_date.'
			</td>	
		</tr>
		<tr class="row">
			<td class="col-xs-3" >
				End Date
			</td>
			<td class="col-xs-3" >
				'. $e_date.'
			</td>	
		</tr>
		<tr class="row">
			<td class="col-xs-3" >
				Duration
			</td>
			<td class="col-xs-3" >
				'. $duration.' day
			</td>	
		</tr>
		<tr class="row">
			<td class="col-xs-3" >
				Project Cost
			</td>
			<td class="col-xs-3" >
				'. $pro_cost.' $
			</td>	
		</tr>
		<tr class="row">
			<td class="col-xs-3" >
				The expected profit from the project
			</td>
			<td class="col-xs-3" >
				'. $exp_prof.' $
			</td>	
		</tr>
		
	</table></div>';
	echo $page;
	echo form_open('project/pdf');
	?>
	<input type="hidden" value='<?php echo $page;?>' name="page" />
	<button class="btn btn-success">Save</button>
	
	<?php
	echo form_close();
	?>
	<button class="btn btn-info" onclick="printDiv('print_area');">Print</button>
	<a href="<?php echo site_url();?>" class="btn btn-danger"> Back </a>
</div>