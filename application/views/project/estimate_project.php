<div class="container">
<?php 
echo form_open('project/estimate_project');
?>
<div class="title"><h3>Please fill out form to create your project schedule</h3></div>
<table class="table col-xs-12 t_style" >
	<tr class="row">
		<td class="col-xs-6" >
			<span>Write Project name:</span>
			<input type="text" name="pro_name" class="form-control" required=""/>
		</td>
		<td class="col-xs-6" >
			<span>Assigned to:</span>
			<input type="text" name="assigned_to"  class="form-control" required=""/>
		</td>
	</tr>
	<tr class="row">
		<td class="col-xs-6" >
			<span>Description:</span><br>
			<textarea name="description" class="form-control" rows="8"></textarea>
		</td>
		<td class="col-xs-6" >
			<p>Chose the goal of your project:</p>
			<input type="radio"  name="pro_goal" value="Freedom from the daily work" required=""/>Freedom from the daily work<br>
			<input type="radio"  name="pro_goal" value="To be independent" required=""/>To be independent<br>
			<input type="radio"  name="pro_goal" value="I practice the work that he loved" required=""/>I practice the work that he loved<br>
			<input type="radio"  name="pro_goal" value="Raise the standard of living" required=""/>Raise the standard of living<br>
			<input type="radio"  name="pro_goal" value="he beginning of the work or service I see it Required" required=""/>he beginning of the work or service I see it Required<br>
			<input type="radio"  name="pro_goal" value="Because it does not currently have a job there" required=""/>Because it does not currently have a job there<br>
			<input type="radio"  name="pro_goal" value="In order to have my fortune future " required=""/>In order to have my fortune future <br>
		</td>
	</tr>
	<tr class="row">
		<td class="col-xs-6" >
			<span>Duration(number of day): </span>
			<input type="number" class="form-control" name="duration" required=""/> Day(s)
		</td>
		
	</tr>
	<tr class="row">
		<td class="col-xs-6" >
			<span>Start date (date): </span>
			<input type="date" class="form-control" name="s_date" required=""/>
		</td>
		<td class="col-xs-6" >
			<span>End date (date): </span>
			<input type="date" class="form-control" name="e_date" required=""/>
		</td>
	</tr>
	<tr class="row">
		<td class="col-xs-6" >
			<span>Cost of your job hourly: ($)</span>
			<input type="number" class="form-control" name="cost_h" required=""/> 
		</td>
		<td class="col-xs-6" >
			<span>Cost at the beginning: ($)</span>
			<input type="number" class="form-control" name="cost_begin" required=""/> 
		</td>
	</tr>
	<tr class="row">
		<td class="col-xs-6" >
			<span>Operation expenses: ($)</span>
			<input type="number" class="form-control" name="op_exp" required=""/> 
		</td>
		<td class="col-xs-6" >
			<span>The value of the expected return of the project: ($) </span>
			<input type="number" class="form-control" name="expected_return" required=""/> 
		</td>
	</tr>
	<tr class="row">
		<td class="col-xs-6" >
			<span>Comments:</span><br />
			<textarea name="comments" class="form-control"></textarea>
		</td>
	</tr>
	<tr class="row">
		<td class="col-xs-6" >
			<input type="reset" value="Clear" style="width: 100%;" class="btn btn-danger"/>
		</td>
		<td class="col-xs-6" >
			<input type="submit" value="Next" style="width: 100%;" name="next"class="btn btn-success" />
		</td>
	</tr>
</table>
</div>
