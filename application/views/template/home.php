<link rel="stylesheet" href="<?php echo base_url();?>files/public/css/style.css">
<script src="<?php echo base_url() ?>files/public/js/Chart.js" type="text/javascript"></script>
<script>

		var doughnutData = [
				{
					value: <?php echo $neg_test; ?>,
					color:"#F7464A",
					highlight: "#FF5A5E",
					label: "Fail Test"
				},
				{
					value: <?php echo $pos_test; ?>,
					color: "#4cae4c",
					highlight: "#58C558",
					label: "Success Test"
				}

			];

			window.onload = function(){
				var ctx = document.getElementById("chart-area").getContext("2d");
				window.myDoughnut = new Chart(ctx).Doughnut(doughnutData, {responsive : true});
			};

			$('#randomizeData').click(function(){
				$.each(doughnutData, function(i, piece){
					doughnutData[i].value = randomScalingFactor();
			    	doughnutData[i].color = 'rgba(' + randomColorFactor() + ',' + randomColorFactor() + ',' + randomColorFactor() + ',.7)';
				});
		    	window.myDoughnut.update();
		    });



	</script>
	

<div class="container">
<h1 style="color: #009933">project schedule  !</h1>
<!--<h4>Free Domain Name ,professional design layout , and point-and-click editing</h4>-->
<h3>project schedule brings project management with smart sheet </h4>
<table class="table col-xs-12">
	<tr class="row">
		<td class="col-xs-6" >
			<img src="<?php echo base_url(); ?>files/public/images/computer.png" />
		</td>
		<td class="col-xs-6">
			  
				<h3 style="color: #009933">Step 1 : <small> click a start button </small></h3>
		        <br />
			 
				<h3 style="color: #333300">Step 2 : <small>Then fill in the form</small></h3>
			    <br /> 
				<h3 style="color: #6699FF">Step 2 : <small>Get your schedule save it or print it</small></h3>
			    <br />
				<a href="<?php echo site_url();?>/project/estimate_project" class="btn btn-danger col-xs-6">Click here to start ></a>
			  

		</td>
		
		
	</tr>
	<tr class="row">
		<td class="col-xs-3">
			<h3>Are you entrepreneur?</h3>
			<h5>If you want to make this test click on the start of the test  <br /><br />
			<a href="<?php echo site_url();?>/test/make_test" class="btn btn-success btn-block"> start </a> </h5>
		</td>
		<td class="col-xs-6">
			<div id="canvas-holder" style="width: 200px; height: 200px; ">
				<canvas id="chart-area" width="200" height="200"/>
			</div>
			<br>
			<p style="padding-left:23px; ">Test Statistical Results</p>
		</td>
	</tr>
	
	</table>
	<div class="row " >
		<div class="col-md-3 col-sm-3 col-xs-12">
			<div class="panel panel-default">
			  <div style="height: 60px;" class="panel-heading">Evaluate your project by: </div>
			  <div style="height: 380px;" class="panel-body">
			    <p>-Your goal of the project</p>
				<p>-Personal Evaluate your recipes</p>
				<p>-Your experience and your skills</p>
				<p>-Your analysis of the labor market</p>
				<p>-Activity site</p>
				<p>-Feasibility</p>
				<p>-Your choice for workers</p>
			  </div>
			</div>
			
		</div>
		<div class="col-md-3 col-sm-3 col-xs-12">
			<div class="panel panel-default">
			  <div style="height: 60px;" class="panel-heading">Do you have a comparative advantage in your idea?</div>
			  <div style="height: 380px;" class="panel-body">
			    <p>Comparative advantage:</p>
				<p>Is any advantage in your project differentiate you from your competitors
				It is a positive element different from competitors and competitors can not easily imitate
				And comparative advantage of the multiple forms that can be tested in your idea</p>
				 <p>1. natural advantages such as location</p>
				 <p>2. operational advantages such as quality</p>
				 <p>3. intellectual advantages such as patents</p>
			  </div>
			</div>
			
		</div>
		<div class="col-md-3 col-sm-3 col-xs-12">
			<div class="panel panel-default">
			  <div style="height: 60px;" class="panel-heading">Three tests important to you:</div>
			  <div style="height: 380px;" class="panel-body">
			    <p>After completion of the work plan tests there can work to make sure there is actually an action plan you have, that can do its job in the event of pass you can be sure that you have a business plan Court: </p>
							The first test: a test to real markets:
				Second test: To test the project competitor:
				Third test: To test that the project is a return:
				If exceeded the previous three tests  you are ready to start your project and if not
							exceed stopped for the start of the project</p>
			  </div>
			</div>
			
		</div>
		<div class="col-md-3 col-sm-3 col-xs-12">
			<div class="panel panel-default">
			  <div style="height: 60px;" class="panel-heading">Launched God's blessing  ...</div>
			  <div style="height: 380px;" class="panel-body">
			    <p>But remember that the success of the first experiment is the wishes of the difficult investigation, all the people have
				failed and gone through failure and success they could then
				And remember that success goes through bad experiences, and we are aware that the tests are unsuccessful and fuel success
				</p>
			  </div>
			</div>
			
		</div>
	</div>
</div>










<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
   <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
    <script src="<?php echo base_url();?>files/public/js/bootstrap.min.js"></script>