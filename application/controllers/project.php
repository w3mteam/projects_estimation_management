<?php
/**
 * 
 */
class Project extends CI_Controller {
	
	function __construct() {
		parent::__construct();
		$this->load->model('data_model');
	}
	
	public function estimate_project()
	{
		$this->form_validation->set_rules('pro_name','Project Name','required');
		$this->form_validation->set_rules('description','Description','required');
		$this->form_validation->set_rules('assigned_to','Assigned to','required');
		$this->form_validation->set_rules('pro_goal','Project Goal','required');
		$this->form_validation->set_rules('duration','Duration','required|numeric');
		$this->form_validation->set_rules('s_date','Start Date','required');
		$this->form_validation->set_rules('e_date','End Date','required');
		$this->form_validation->set_rules('cost_h','Cost per hour','required|numeric');
		$this->form_validation->set_rules('cost_begin','Cost at the beginning','required|numeric');
		$this->form_validation->set_rules('op_exp','Operation expenses','required|numeric');
		$this->form_validation->set_rules('expected_return','The value of the expected return of the project','required|numeric');
		$this->form_validation->set_rules('comments','Comments','required');
		if($this->form_validation->run() === TRUE){
			//$data['message'] = $this->calculate_result($_POST);
			$data['pro_name'] = $this->input->post('pro_name');
			$data['description'] = $this->input->post('description');
			$data['pro_goal'] = $this->input->post('pro_goal');
			$data['assigned_to'] = $this->input->post('assigned_to');
			$data['s_date'] = $this->input->post('s_date');
			$data['e_date'] = $this->input->post('e_date');
			$data['duration'] = $this->input->post('duration');
			$data['pro_cost'] = $this->input->post('cost_h')+
			$this->input->post('cost_begin')+$this->input->post('op_exp');
			$data['exp_prof'] = $this->input->post('expected_return') - $data['pro_cost'];
			
			$data['page'] = 'project/project_result';
			$this->load->view('view_controller',$data);
		}
		else {
			
			$data['page'] = 'project/estimate_project';
			$this->load->view('view_controller',$data);
		}
	}
	function pdf()
	{
		$this->form_validation->set_rules('page','Page','required');
		if($this->form_validation->run() === TRUE){
		    $this->load->helper('pdf_helper');
		    $data['page_data'] = $this->input->post('page');
		    $this->load->view('project/pdfreport', $data);
		}
	}
}
