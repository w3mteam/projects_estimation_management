<?php

/**
 * 
 */
class Main extends CI_Controller {
	
	function __construct() {
		parent::__construct();
		$this->load->library('support_lib');
	}
	
	public function index()
	{
		$this->load->model('data_model');
		$data['pos_test'] = $this->data_model->get_test_result('positive');
		$data['neg_test'] = $this->data_model->get_test_result('negative');
		$data['page'] = 'template/home';
		$this->support_lib->view_template($data);
	}
	
}
