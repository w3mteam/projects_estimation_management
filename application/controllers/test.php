<?php
/**
 * 
 */
class Test extends CI_Controller {
	
	function __construct() {
		parent::__construct();
		$this->load->model('data_model');
	}
	
	public function make_test()
	{
		$this->form_validation->set_rules('send','Send','required');
		if($this->form_validation->run() === TRUE){
			$data['message'] = $this->calculate_result($_POST);
			$data['page'] = 'test/test';
			$this->load->view('view_controller',$data);
		}
		else {
			
			$data['questions'] = $this->data_model->get_questions();
			$data['page'] = 'test/test';
			$this->load->view('view_controller',$data);
		}
	}
	private function calculate_result($res='')
	{
		$positive = 0;
		$negative = 0;
		if(!empty($res)){
			foreach ($res as $key => $row) {
				if($row == 0){
					$negative++;
				}
				elseif ($row == 1) {
					$positive++;
				}
			}
			
			if($positive >= 7){
				$this->data_model->increment_test_result('positive');
				return "You have a recipe for the initiative and prepared to be the owners of a successful initiative I'm not ready to be the owners of the initiative";
			}
			else{
				$this->data_model->increment_test_result('negative');
				return "Stop and do not take the initiative and then review  yourself before it initiates";
			}
		}
		else {
			return "No data..";
		}
	}
}
