<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

 
/* Paper Section */
$lang['description'] = 'Description';
$lang['title'] = 'Title';
$lang['type'] = 'Type';
$lang['success_add'] = 'Information added successfully';
$lang['success_edit'] = 'Information edited successfully';
$lang['fail_add'] = 'Adding information failed';
$lang['fail_edit'] = 'Editing information failed';
$lang['faild_delete'] = 'Deleting information failed';
$lang['no_privilages'] = "403 You don't have privilages";
$lang['invalid_id'] = "Invalid Item ID";
$lang['assign_paper_not_msg'] = 'You have a paper assigned to you to judging';
$lang['failed_assign'] = 'assign paper failed';
$lang['success_assign'] = 'paper assigned successfully';
